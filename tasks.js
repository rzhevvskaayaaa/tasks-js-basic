// Solutions for homework tasks

// Insert Task 1 solution here
/**
 * @param {string[]} strs
 * @returns {string}
 */
function longestCommonPrefix(strs) {
    if (strs.length === 0) return "";

    let prefix = strs[0];

    for (let i = 1; i < strs.length; i++) {
        while (strs[i].indexOf(prefix) !== 0) {
            prefix = prefix.substring(0, prefix.length - 1);
            if (prefix === "") return "";
        }
    }

    return prefix;
}
// Insert Task 2 solution here
/**
 * @param {number} n
 * @returns {number}
 */
function countPrimes(n) {
    if (n <= 2) return 0;

    let primesCount = 0;
    const isPrime = new Array(n).fill(true);
    isPrime[0] = isPrime[1] = false;

    for (let i = 2; i * i < n; i++) {
        if (isPrime[i]) {
            for (let j = i * i; j < n; j += i) {
                isPrime[j] = false;
            }
        }
    }

    for (let i = 2; i < n; i++) {
        if (isPrime[i]) {
            primesCount++;
        }
    }

    return primesCount;
}
// Insert Task 3 solution here
/**
 * @param {string} s
 * @returns {number}
 */
const romanToInt = (s) => {
    const romanToIntMap = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    };

    let total = 0;
    let prevValue = 0;

    for (let i = s.length - 1; i >= 0; i--) {
        const char = s[i];
        const value = romanToIntMap[char];

        if (value >= prevValue) {
            total += value;
        } else {
            total -= value;
        }

        prevValue = value;
    }

    return total;
};
// Insert Task 4 solution here
/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 */
function merge(nums1, m, nums2, n) {
    let p1 = m - 1;
    let p2 = n - 1;
    let p = m + n - 1;

    while (p1 >= 0 && p2 >= 0) {
        if (nums1[p1] > nums2[p2]) {
            nums1[p] = nums1[p1];
            p1--;
        } else {
            nums1[p] = nums2[p2];
            p2--;
        }
        p--;
    }

    while (p2 >= 0) {
        nums1[p] = nums2[p2];
        p2--;
        p--;
    }
}
// Insert Task 5 solution here
class Solution {
    /**
     * @param {number[]} nums
     */
    constructor(nums) {
        this.nums = nums;
        this.original = [...nums];
    }

    /**
     * @returns {number[]}
     */
    reset() {
        this.nums = [...this.original];
        return this.nums;
    }

    /**
     * @returns {number[]}
     */
    shuffle() {
        let n = this.nums.length;
        for (let i = n - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.nums[i], this.nums[j]] = [this.nums[j], this.nums[i]];
        }
        return this.nums;
    }
}